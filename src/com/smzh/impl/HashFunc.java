package com.smzh.impl;

/**
 * 自定义hash算法
 * 
 * @author jun
 *         FNV-1a和FNV-1的唯一区别就是xor和multiply的顺序不同，他们所采用的FNV_prime和offset_basis都相同，
 *         有人认为FNV-1a在进行小数据（小于4个字节）哈希时有更好的性能。
 *
 */
public interface HashFunc {
	final int FNV_PRIME = 2 ^ 24 + 2 ^ 8 + 0x93;// 16777619;// FNV用于散列的质数 32位
	final int HASH = (int) 2166136261L;// 初始的hash

	public Integer hash(Object key);
}
