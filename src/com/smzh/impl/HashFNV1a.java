package com.smzh.impl;

/*FNV-1a描述：

    hash = offset_basis 

    for each octet_of_data to be hashed

        hash = hash xor octet_of_data

        hash = hash * FNV_prime

    return hash

*/
public class HashFNV1a implements HashFunc {

	@Override
	public Integer hash(Object key) {
		String data = key.toString();
		// 默认使用FNV1a hash算法
		int hash = HASH;// 初始的hash
		for (int i = 0; i < data.length(); i++)
			hash = (hash ^ data.charAt(i)) * FNV_PRIME;
		hash += hash << 13;// 改进
		hash ^= hash >> 7;
		hash += hash << 3;
		hash ^= hash >> 17;
		hash += hash << 5;
		return hash;
	}

}
