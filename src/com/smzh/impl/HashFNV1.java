package com.smzh.impl;

/**
 * FNV能快速hash大量数据并保持较小的冲突率，它的高度分散使它适用于hash一些非常相近的字符串，比如URL，hostname，<br>
 * 文件名，text，IP地址等<br>
 * 
 * 
 * 算法描述：<br>
 * 相关变量：<br>
 * hash值：一个n位的unsigned int型hash值<br>
 * offset_basis：初始的哈希值<br>
 * FNV_prime：FNV用于散列的质数<br>
 * octet_of_data：8位数据（即一个字节）<br>
 * FNV-1描述：<br>
 * hash = offset_basis<br>
 * for each octet_of_data to be hashed<br>
 * hash = hash * FNV_prime<br>
 * hash = hash xor octet_of_data<br>
 * return hash<br>
 * 
 * FNV_prime的取值： <br>
 * 32 bit FNV_prime = 2^24 + 2^8 + 0x93 = 16777619<br>
 * 64 bit FNV_prime = 2^40 + 2^8 + 0xb3 = 1099511628211<br>
 * 128 bit FNV_prime = 2^88 + 2^8 + 0x3b = 309485009821345068724781371<br>
 * 256 bit FNV_prime = 2^168 + 2^8 + 0x63
 * =374144419156711147060143317175368453031918731002211<br>
 * 
 * offset_basis的取值 <br>
 * 32 bit offset_basis = 2166136261 <br>
 * 64 bit offset_basis = 14695981039346656037 <br>
 * 128 bit offset_basis = 144066263297769815596495629667062367629 <br>
 * 256 bit offset_basis =
 * 100029257958052580907070968620625704837092796014241193945225284501741471925557
 * <br>
 */
public class HashFNV1 implements HashFunc {

	@Override
	public Integer hash(Object key) {
		String data = key.toString();
		// 默认使用FNV1hash算法
		int hash = HASH;// 初始的hash
		for (int i = 0; i < data.length(); i++)
			hash = (hash * FNV_PRIME) ^ data.charAt(i);
		hash += hash << 13;// 改进
		hash ^= hash >> 7;
		hash += hash << 3;
		hash ^= hash >> 17;
		hash += hash << 5;
		return hash;
	}

}
