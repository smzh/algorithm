package com.smzh.service;

import com.smzh.bean.ListNode;

/**
 * 链表删除
 * 
 * @author jun
 *
 */

public class DeleteLinkedNode {

	public ListNode removeElements(ListNode head, int val) {

		   ListNode dummy = new ListNode(0);  
	        dummy.next = head;  
	        ListNode p = dummy;  
	        ListNode q = head;  
	        while(q!=null) {  
	            if(q.val == val) {  
	                p.next = q.next;  
	            } else {  
	                p = p.next;  
	            }  
	            q = q.next;  
	        }  
	          
	        return dummy.next;  

	}

	public ListNode removeElements2(ListNode head, int val) {
		// Write your code here
		while (head != null && head.val == val) {
			head = head.next;
		}
		if (head == null) {
			return null;
		}
		ListNode result = head;
		ListNode q = result;
		ListNode p = head.next;
		while (p != null) {
			if (p.val == val) {
				q.next = p.next;
			} else {
				q = q.next;
			}
			p = p.next;
		}
		return result;
	}
}
