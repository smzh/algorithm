package com.smzh.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.SortedMap;
import java.util.TreeMap;

import com.smzh.impl.HashFNV1;
import com.smzh.impl.HashFNV1a;
import com.smzh.impl.HashFunc;

/**
 * hash一致性
 * 
 * @author jun
 *
 * @param <T>
 */
public class ConsistentHash<T> {

	/** Hash计算对象，用于自定义hash算法 */
	HashFunc hashFunc;
	/** 复制的节点个数 */
	private final int numberOfReplicas;
	/** 一致性Hash环 */
	private final SortedMap<Integer, T> circle = new TreeMap<Integer, T>();

	/**
	 * 构造
	 * 
	 * @param hashFunc
	 *            hash算法对象
	 * @param numberOfReplicas
	 *            复制的节点个数，增加每个节点的复制节点有利于负载均衡
	 * @param nodes
	 *            节点对象
	 */
	public ConsistentHash(HashFunc hashFunc, int numberOfReplicas, Collection<T> nodes) {
		this.numberOfReplicas = numberOfReplicas;
		this.hashFunc = hashFunc;
		// 初始化节点
		for (T node : nodes) {
			add(node);
		}
	}

	/**
	 * 增加节点<br>
	 * 每增加一个节点，就会在闭环上增加给定复制节点数<br>
	 * 例如复制节点数是2，则每调用此方法一次，增加两个虚拟节点，这两个节点指向同一Node
	 * 由于hash算法会调用node的toString方法，故按照toString去重
	 * 
	 * @param node
	 *            节点对象
	 */
	public void add(T node) {
		for (int i = 0; i < numberOfReplicas; i++) {
			circle.put(hashFunc.hash(node.toString() + i), node);
		}
	}

	/**
	 * 移除节点的同时移除相应的虚拟节点
	 * 
	 * @param node
	 *            节点对象
	 */
	public void remove(T node) {
		for (int i = 0; i < numberOfReplicas; i++) {
			circle.remove(hashFunc.hash(node.toString() + i));
		}
	}

	/**
	 * 获得一个最近的顺时针节点
	 * 
	 * @param key
	 *            为给定键取Hash，取得顺时针方向上最近的一个虚拟节点对应的实际节点
	 * @return 节点对象
	 */
	public T get(Object key) {
		if (circle.isEmpty()) {
			return null;
		}
		int hash = hashFunc.hash(key);
		if (!circle.containsKey(hash)) {
			SortedMap<Integer, T> tailMap = circle.tailMap(hash); // 返回此映射的部分视图，其键大于等于
																	// hash
			hash = tailMap.isEmpty() ? circle.firstKey() : tailMap.firstKey();
		}
		// 正好命中
		return circle.get(hash);
	}

	public static String getRandomString(int length) { // length表示生成字符串的长度

		String base = "abcdefghijklmnopqrstuvwxyz0123456789";

		Random random = new Random();

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			int number = random.nextInt(base.length());
			sb.append(base.charAt(number));
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		List<String> nodes = new ArrayList<>();
		nodes.add("node1");
		nodes.add("node2");
		nodes.add("node3");
		nodes.add("node4");
		nodes.add("node5");
		nodes.add("sfdfvdfcvjkl");
		nodes.add("asdfr");
		nodes.add("fhgbvrf");
		int numberOfReplicas = 100000;
		int total = 100;
		ConsistentHash<String> hashFvn1 = new ConsistentHash<>(new HashFNV1(), numberOfReplicas, nodes);

		ConsistentHash<String> hashFvn1a = new ConsistentHash<>(new HashFNV1a(), numberOfReplicas, nodes);
		Map<String, Integer> counterFvn1 = new HashMap<>();
		Map<String, Integer> counterFvna = new HashMap<>();
		for (int i = 0; i < total; i++) {
			String key = getRandomString(20);// 产生20位长度的随机字符串
			String node1 = hashFvn1.get(key);
			Integer count1 = counterFvn1.get(node1);
			if (count1 == null) {
				counterFvn1.put(node1, 1);
			} else {
				counterFvn1.put(node1, count1 + 1);
			}
			
			String node1a = hashFvn1a.get(key);
			Integer count1a = counterFvn1.get(node1a);
			
			if (count1a == null) {
				counterFvna.put(node1a, 1);
			} else {
				counterFvna.put(node1a, count1a + 1);
			}
		}
		
		System.out.println("=================FVN1 hash 算法==============");
		for (String key : counterFvn1.keySet()) {
			Integer c = counterFvn1.get(key);
			System.out.println("节点名:" + key + "\t命中次数:" + c + "\t命中率:" + ((c * 1.0) / total) * 100 + "%");
		}
		
		
		System.out.println("=================FVN1a hash 算法==============");
		for (String key : counterFvna.keySet()) {
			Integer c = counterFvna.get(key);
			System.out.println("节点名:" + key + "\t命中次数:" + c + "\t命中率:" + ((c * 1.0) / total) * 100 + "%");
		}
		// System.out.println(counter);
	}
}
